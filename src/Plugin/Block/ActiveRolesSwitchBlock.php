<?php

namespace Drupal\active_role_switch\Plugin\Block;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;

/**
 * Provides a 'ActiveRolesSwitch' block.
 *
 * @Block(
 *   id = "active_role_switch_role_form",
 *   admin_label = @Translation("Active Role Switch Form Block"),
 *   category = @Translation("Active role switch form block")
 * )
 */
class ActiveRolesSwitchBlock extends BlockBase implements ContainerFactoryPluginInterface {
  
    /**
     * @formBuilder
     */
    protected $formBuilder;

    /**
     * 
     */
    public function __construct(array $configuration, $plugin_id, $plugin_definition, FormBuilderInterface $formBuilder) {
        parent::__construct($configuration, $plugin_id, $plugin_definition);
        $this->formBuilder = $formBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
        return new static(
                $configuration, $plugin_id, $plugin_definition, $container->get('form_builder')
        );
    }
    
  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = $this->formBuilder->getForm('Drupal\active_role_switch\Form\ActiveRoleSwitchForm');
    return $form;
  }

}
