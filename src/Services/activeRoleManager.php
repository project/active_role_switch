<?php

namespace Drupal\active_role_switch\Services;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Description of activeRoleManager
 *
 * @author pankaj
 */
class activeRoleManager {

  /**
   * Drupal\Core\Entity\EntityTypeManagerInterface definition.
   *
   * @var Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  

  /**
   * {@inheritdoc}
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * Get current active role in the user account
   * @return type
   */
  public function getActiveRole($uid) {
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    $role = $user->active_role->value;
    return $role;
  }

  /**
   * Switch Active Role and update user field
   * @param type $uid
   * @param type $role
   */
  public function switchActiveRole($uid, $role) {
    $user = $this->entityTypeManager->getStorage('user')->load($uid);
    $user->set('active_role', $role);
    $r = $user->save();
    return $r;
  }

  /**
   * Get all roles lists
   */
  public function getAllRoles() {
    $roles = $this->entityTypeManager->getStorage('user_role')->loadMultiple();
    $rolesArray = [];
    foreach ($roles as $r) {
      $role_id = $r->id();
      $role_label = $r->label();
      // eliminate ignored roles
      if (!in_array($role_id, IGNORED_ROLES)) {
        $rolesArray[$role_id] = $role_label;
      }
    }
    return $rolesArray;
  }

}
