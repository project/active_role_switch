<?php

namespace Drupal\active_role_switch\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Description of ActiveRoleSwitchForm
 *
 * @author pankaj
 */
class ActiveRoleConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'active_role_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['active_role_switch.active_role_settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('active_role_switch.active_role_settings');
    // Field title
    $form['active_role_field_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Field Label'),
      '#default_value' => !empty($config->get('active_role_field_title')) ? $config->get('active_role_field_title') : 'Active Role',
    ];
    // Field type
    $form['active_role_field_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Field Type'),
      '#options' => [
        'select' => 'Select Box',
        'radios' => 'Radio Buttons'
      ],
      '#default_value' => !empty($config->get('active_role_field_type')) ? $config->get('active_role_field_type') : 'select',
    ];

    $form['active_role_form_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Active Role Form Wrapper Class'),
      '#default_value' => $config->get('active_role_form_class'),
    ];
    
    $form['active_role_field_class'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Active Role Field Wrapper Class'),
      '#default_value' => $config->get('active_role_field_class'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Get all form values
    $active_role_field_title = $form_state->getValue('active_role_field_title');
    $active_role_field_type = $form_state->getValue('active_role_field_type');
    $active_role_form_class = $form_state->getValue('active_role_form_class');
    $active_role_field_class = $form_state->getValue('active_role_field_class');

    // Save config values
    $this->config('active_role_switch.active_role_settings')
      ->set('active_role_field_title', $active_role_field_title)
      ->set('active_role_field_type', $active_role_field_type)
      ->set('active_role_form_class', $active_role_form_class)
      ->set('active_role_field_class', $active_role_field_class)
      ->save();
  }

}
