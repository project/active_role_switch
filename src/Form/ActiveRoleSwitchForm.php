<?php

namespace Drupal\active_role_switch\Form;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use Drupal\field\Entity\FieldConfig;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\active_role_switch\Services\activeRoleManager;
use Drupal\Core\Entity\EntityManager;

/**
 * Description of ActiveRoleSwitchForm
 *
 * @author pankaj
 */
class ActiveRoleSwitchForm extends FormBase {

  /**
   * The Active Role Manager.
   *
   * @var \Drupal\active_role_switch\Services\activeRoleManager
   */
  protected $activeRoleManager;

  /**
   * Drupal\Core\Entity\EntityManager definition.
   *
   * @var Drupal\Core\Entity\EntityManager
   */
  private $entityManager;

  public function __construct(activeRoleManager $activeRoleManager, EntityManager $entityManager) {
    $this->activeRoleManager = $activeRoleManager;
    $this->entityManager = $entityManager;
  }

  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('active_role_switch.active_role_manager'), $container->get('entity.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'active_role_switch_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get all assigned role to
    $current_user = $this->currentUser();
    $current_uid = $current_user->id();

    // get all user roles
    $userRoles = $current_user->getRoles();
    // get current active role
    $active_role = $this->activeRoleManager->getActiveRole($current_uid);

    // get all available roles list
    $allRoles = $this->activeRoleManager->getAllRoles();

    $role_array = [];

    foreach ($userRoles as $r) {
      if (key_exists($r, $allRoles)) {
        $role_array[$r] = $allRoles[$r];
      }
    }
    
    // Load configuratin field values
    $config = \Drupal::config('active_role_switch.active_role_settings');
    $field_label = $config->get('active_role_field_title');
    $field_type = $config->get('active_role_field_type');
    $form_class = !empty($config->get('active_role_form_class')) ? $config->get('active_role_form_class') : '';
    $field_class = !empty($config->get('active_role_field_class')) ? $config->get('active_role_field_class') : '';

    $form['active_user_roles'] = [
      '#type' => !empty($field_type) ? $field_type : 'select',
      '#title' => !empty($field_label) ? t($field_label) : t('Active Role'),
      '#options' => $role_array,
      '#ajax' => [
        'event' => 'change',
        'wrapper' => 'edit-active-user-roles',
        'callback' => '::_switch_user_role',
      ],
      '#default_value' => $active_role,
      '#attributes' => ['class' => [$field_class]]
    ];
    
    // add class
    $form['#attributes'] = ['class' => [$form_class]];

    return $form;
  }

  public function _switch_user_role(array &$form, FormStateInterface $form_state) {
    // Get Current UID
    $uid = $this->currentUser()->id();
    // Get Selected Role
    $selected_roles = $form_state->getValue('active_user_roles');
    // update user role in active_role field
    $r = $this->activeRoleManager->switchActiveRole($uid, $selected_roles);
    if (!empty($r)) {
      // Redirect user to home page
      $url = "/";
      $response = new AjaxResponse();
      $command = new RedirectCommand($url);
      $response->addCommand($command);
      return $response;
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    
  }

}
