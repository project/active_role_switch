<?php

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\Entity\FieldConfig;
use Drupal\user\Entity\Role;
use Drupal\active_role_switch\Services\activeRoleManager;

// Define array of ignored roles
define('IGNORED_ROLES', [
  'anonymous' => 'anonymous',
  'authenticated' => 'authenticated',
]);

function active_role_switch_install() {

  $bundles = ['user'];

  $fields['active_role'] = [
    'type' => 'list_string',
    'entity_type' => 'user',
    'bundle' => 'user',
    'label' => 'Active Role',
    'description' => 'Active role',
    'required' => FALSE,
    'widget' => [
      'type' => 'options_select',
    ],
    'storage' => [
      'allowed_values' => [],
      'allowed_values_function' => '_get_active_role_options'
    ]
  ];

  foreach ($fields as $field_name => $config) {
    $field_storage = FieldStorageConfig::loadByName($config['entity_type'], $field_name);
    if (empty($field_storage)) {
      FieldStorageConfig::create(array(
        'field_name' => $field_name,
        'entity_type' => $config['entity_type'],
        'type' => $config['type'],
      ))->save();
    }
  }

  foreach ($bundles as $bundle) {
    foreach ($fields as $field_name => $config) {
      $config_array = array(
        'field_name' => $field_name,
        'entity_type' => $config['entity_type'],
        'bundle' => $bundle,
        'label' => $config['label'],
        'required' => $config['required'],
      );

      if (isset($config['settings'])) {
        $config_array['settings'] = $config['settings'];
      }

      $field = FieldConfig::loadByName($config['entity_type'], $bundle, $field_name);
      if (empty($field) && $bundle !== "" && !empty($bundle)) {
        FieldConfig::create($config_array)->save();
      }

      if ($config['storage'] && $config['storage']['allowed_values_function']) {
        //load field
        $field_purchasers = FieldStorageConfig::loadByName('user', $field_name);
        $field_purchasers->setSetting('allowed_values_function', $config['storage']['allowed_values_function']);
        //save configuration
        $field_purchasers->save();
      }

      if ($bundle !== "" && !empty($bundle)) {
        if (!empty($field)) {
          $field->setLabel($config['label'])->save();
          $field->setRequired($config['required'])->save();
        }
        if ($config['widget']) {
          entity_get_form_display($config['entity_type'], $bundle, 'default')
            ->setComponent($field_name, $config['widget'])
            ->save();
        }
        if ($config['formatter']) {
          foreach ($config['formatter'] as $view => $formatter) {
            $view_modes = \Drupal::entityManager()->getViewModes($config['entity_type']);
            if (isset($view_modes[$view]) || $view == 'default') {
              entity_get_display($config['entity_type'], $bundle, $view)
                ->setComponent($field_name, !is_array($formatter) ? $config['formatter']['default'] : $formatter)
                ->save();
            }
          }
        }
      }
    }
  }
}

/**
 * Get all roles list
 * @return type
 */
function _get_active_role_options() {
  $rolesArrayFn = \Drupal::service('active_role_switch.active_role_manager');
  return $rolesArrayFn->getAllRoles();
}
