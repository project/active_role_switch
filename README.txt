CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Configuration
 * Usage
 * Installation
 * Maintainers


INTRODUCTION
------------

This module is helpful if you have any custom functionality based on the assigned multiple roles

The value of active role gets saved in user entity and will be available dynamically in all references of the users

The module creates a custom 'active_role' field and a block with a role switcher form

The form allows user to switch their active role

CONFIGURATION
-------------

A configuration form is available for customizing and styling the form

Path: admin/config/active-role-settings


Usage:
------

Active role field will be available in user entity and can be used in views, themes etc.

So that any custom views and altered theme would be there based on the selected active role


INSTALLATION
------------

Install as you would normally install a contributed Drupal module. See:
https://drupal.org/documentation/install/modules-themes/modules-8 for further
information.


MAINTAINERS
-----------

Current maintainers:
 * Pankaj Gupta - https://www.drupal.org/u/pankajg

